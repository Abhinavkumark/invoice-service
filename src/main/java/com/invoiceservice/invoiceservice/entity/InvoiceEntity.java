package com.invoiceservice.invoiceservice.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
public class InvoiceEntity {

	@Id
	@Column(name = "invoiceNumber")
	private String invoiceNumber;
	
	@Column(name = "invoiceDate")
	@Temporal(TemporalType.DATE)
	private Date invoiceDate;
	
	@Column(name = "paymentTerm")
	private String paymentTerm ;
	
	@Column(name = "status")
	private String status ;
}

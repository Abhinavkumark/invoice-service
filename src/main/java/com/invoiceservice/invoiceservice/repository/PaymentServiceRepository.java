package com.invoiceservice.invoiceservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.invoiceservice.invoiceservice.entity.PaymentServiceEntity;

public interface PaymentServiceRepository extends JpaRepository<PaymentServiceEntity, Integer> {

	PaymentServiceEntity findByCode(String paymentTerm);

}

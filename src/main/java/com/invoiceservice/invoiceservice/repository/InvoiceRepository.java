package com.invoiceservice.invoiceservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.invoiceservice.invoiceservice.entity.InvoiceEntity;

@Repository
public interface InvoiceRepository extends JpaRepository<InvoiceEntity, String> {

	@Query(value = "select * from INVOICE_ENTITY  inv inner join PAYMENT_SERVICE_ENTITY  pse on inv.payment_term = pse.code where inv.status = 'UNPAID' and inv.invoice_date =  DATEADD(DAY,-(pse .days-pse.reminder_before_days) , CURRENT_DATE())",nativeQuery= true)
	List<InvoiceEntity> findByStatus(String string);

}

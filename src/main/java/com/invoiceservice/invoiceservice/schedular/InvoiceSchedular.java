package com.invoiceservice.invoiceservice.schedular;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.invoiceservice.invoiceservice.entity.InvoiceEntity;
import com.invoiceservice.invoiceservice.repository.InvoiceRepository;

@Component
public class InvoiceSchedular {

	private static final Logger logger = LoggerFactory.getLogger(InvoiceSchedular.class);

	@Autowired
	private InvoiceRepository invoiceRepository;

	@Scheduled(cron = "0 0 1 * * *")
	public void sendMailForInvoiceRemainder() {
		try {
			List<InvoiceEntity> unpaidInvoices = invoiceRepository.findByStatus("UNPAID");
			for (InvoiceEntity inv : unpaidInvoices) {
				logger.info("Reminder sent for Invoice " + inv.getInvoiceNumber());
			}
		} catch (Exception exc) {
			logger.error(exc.getMessage());
		}
	}
}

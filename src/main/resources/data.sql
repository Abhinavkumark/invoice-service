insert into invoice_entity (invoice_number,invoice_date,payment_term,status)
values ('INV-001','2021-02-10','NET 30','UNPAID');

insert into invoice_entity (invoice_number,invoice_date,payment_term,status)
values ('INV-002','2021-03-13','NET 45','PAID');

insert into invoice_entity (invoice_number,invoice_date,payment_term,status)
values ('INV-003','2021-02-11','NET 30','UNPAID');

insert into invoice_entity (invoice_number,invoice_date,payment_term,status)
values ('INV-004','2021-02-12','NET 30','UNPAID');

insert into invoice_entity (invoice_number,invoice_date,payment_term,status)
values ('INV-005','2021-02-13','NET 30','UNPAID');

insert into payment_service_entity (code,creation_date,days,description,reminder_before_days)
values ('NET 30',CURRENT_DATE(),'30','Within 30 days',4);

insert into payment_service_entity (code,creation_date,days,description,reminder_before_days)
values ('NET 45',CURRENT_DATE(),'45','Within 45 days',5);

insert into payment_service_entity (code,creation_date,days,description,reminder_before_days)
values ('NET 15',CURRENT_DATE(),'15','Within 15 days',2);